package test.edu.epam.likeit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
@RunWith(Suite.class)
@Suite.SuiteClasses({
        UserServiceTest.class
})
public class JUnitTestSuite {
}
