package test.edu.epam.likeit;

import edu.epam.likeit.connectionpool.ConnectionPoolConfiguration;
import edu.epam.likeit.dao.AnswerDAO;
import edu.epam.likeit.dao.DAOException;
import edu.epam.likeit.dao.MarksDAO;
import edu.epam.likeit.service.ServiceException;
import edu.epam.likeit.util.MD5Digest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

public class ConnectionPoolConfigurationTest {
    public static final Logger LOG = LogManager.getLogger(ConnectionPoolConfigurationTest.class);
    @Test
    public void getProperty() throws Exception {
        Assert.assertEquals(new ConnectionPoolConfiguration().getProperty("url"), "jdbc:mysql://localhost:3306/likeit");
    }


    @Test
    public void testContentDAO() throws DAOException {
        System.out.println(111);

    }

    @Test
    public void testAnswerDAO() throws DAOException, ServiceException {
        AnswerDAO dao = new AnswerDAO();
        System.out.print(dao.findByQuestionId(1).getFirst().getAnswer());

    }

    @Test
    public void testChangeRating() throws DAOException {
        AnswerDAO dao = new AnswerDAO();
        dao.changeRating(false, 1);
    }

    @Test
    public void testCheckMark() throws DAOException {
        MarksDAO dao = new MarksDAO();
        dao.checkMark(1,1,true);
    }

    @Test
    public void testMD5() {
        MD5Digest md5 = new MD5Digest();
        System.out.println(md5.encrypt("xxxx"));
        LOG.info("success");
    }

}