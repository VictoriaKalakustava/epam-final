package edu.epam.likeit.connectionpool;

/**
 * Exception which is thrown when any problems with
 * {@link ConnectionPool}.
 */
public class ConnectionPoolException extends Exception {
    ConnectionPoolException() {
    }

    ConnectionPoolException(String message) {
        super(message);
    }

    ConnectionPoolException(String message, Throwable cause) {
        super(message, cause);
    }

    ConnectionPoolException(Throwable cause) {
        super(cause);
    }

    public ConnectionPoolException(String message,
                                   Throwable cause,
                                   boolean enableSuppression,
                                   boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
