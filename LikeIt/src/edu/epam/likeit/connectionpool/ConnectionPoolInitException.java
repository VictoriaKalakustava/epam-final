package edu.epam.likeit.connectionpool;

/**
 * Unchecked exception which is thrown when any problems with initializing
 * {@link ConnectionPool} arise.
 */
public class ConnectionPoolInitException  extends RuntimeException {
    private static final long serialVersionUID = -4361500870819953644L;

    ConnectionPoolInitException() {
    }

    ConnectionPoolInitException(String message) {
        super(message);
    }

    ConnectionPoolInitException(String message, Throwable cause) {
        super(message, cause);
    }

    ConnectionPoolInitException(Throwable cause) {
        super(cause);
    }

    ConnectionPoolInitException(String message,
                                Throwable cause,
                                boolean enableSuppression,
                                boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
