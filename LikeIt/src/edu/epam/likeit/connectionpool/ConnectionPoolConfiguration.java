package edu.epam.likeit.connectionpool;

import java.util.ResourceBundle;

/**
 * Class incapsulating logic of getting {@link ConnectionPool's} init parameters
 * from properties files.
 */
public class ConnectionPoolConfiguration {
    private static final String PATH_TO_PROPERTIES = "properties.database";
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle(PATH_TO_PROPERTIES);
    private static String url = getProperty("url");
    private static String user = getProperty("user");
    private static String password = getProperty("password");
    private static String poolCapacity = getProperty("pool.capacity");
    private static String timeWait = getProperty("time.wait");

    public ConnectionPoolConfiguration() {
    }

    /**
     * @return URL of database
     */
    static String getUrl() {
        return url;
    }

    /**
     * @return database name
     */
    static String getUser() {
        return user;
    }

    /**
     * @return database password
     */
    static String getPassword() {
        return password;
    }

    /**
     * @return capacity of {@link ConnectionPool}
     */
    static int getPoolCapacity() {
        return Integer.valueOf(poolCapacity);
    }

    /**
     * @return time, after which {@link ConnectionPool} stops waiting
     * for Connection from DriverManager
     */
    static int getTimeWait() {
        return Integer.valueOf(timeWait);
    }

    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
