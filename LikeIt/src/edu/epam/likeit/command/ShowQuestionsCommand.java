package edu.epam.likeit.command;

import edu.epam.likeit.entity.Question;
import edu.epam.likeit.service.QuestionService;
import edu.epam.likeit.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Pass questions information from database to the servlet.
 */
public class ShowQuestionsCommand implements Command{
    private static final String REQUEST_ATTR_QUESTIONS = "questions";
    private static final String URL_DEFAULT = "/controller?command=empty";
    private static final String URL_ALL_QUESTIONS = "/index.jsp";
    private static final String COMMAND = "showallquestions";
    private static final Logger LOG = LogManager.getLogger();

    public Command getCommand(HttpServletRequest request) throws CommandException {
        if(request.getParameter("command").equals(COMMAND)) {
            return new ShowQuestionsCommand();
        } else {
            return new ShowQuestion().getCommand(request);
        }
    }

    /**
     * Pass questions information from the database to the servlet
     * and forward to the question page.
     * @param request request from client
     * @param response HttpServletResponse
     * @return new page's URL.
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        try {
            List<Question> allQuestions = new LinkedList<>();
            allQuestions = QuestionService.findAll();
            LOG.debug("RETURN TO COMMAND");
            request.setAttribute(REQUEST_ATTR_QUESTIONS, allQuestions);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return Optional.of(URL_ALL_QUESTIONS);
    }
}
