package edu.epam.likeit.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * Defines interface for all commands.<p>
 *
 * Command is some entity that performs certain actions,
 * e.g. defining next page's URL, updating session
 * or modifying information in the database.<p>
 */
public interface Command {
    /**
     * Defines is the command must execute this request.
     *
     * @param request request from client
     * @return Command which must execute request
     */
    Command getCommand(HttpServletRequest request)
            throws CommandException;

    /**
     * Executes defined command: it does all necessary business logic,
     * sets all request and session attributes etc.
     * and makes a decision of URL used for the next forward or redirect.<p>
     *
     * @param request request from client
     * @param response HttpServletResponse
     * @return Optional<String> with next page's URL
     * or Optional.empty() if this command writes next page's URL
     * @throws CommandException in case if any problems occur,
     * such as processing wrong request parameters and so on
     */
    Optional<String> execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException;

    /**
     * Defines if this command needs redirect after it's execution.
     * Returns false by default.<p>
     *
     * @see Command#execute(HttpServletRequest, HttpServletResponse)
     */
    default boolean needsRedirect() {
        return false;
    }
}
