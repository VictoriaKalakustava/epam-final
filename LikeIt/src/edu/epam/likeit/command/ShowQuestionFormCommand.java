package edu.epam.likeit.command;


import edu.epam.likeit.entity.Theme;
import edu.epam.likeit.service.ServiceException;
import edu.epam.likeit.service.ThemeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Redirect to the page where user can create new question.
 */
public class ShowQuestionFormCommand implements Command{
    private static final String REQUEST_ATTR_THEMES = "themes";
    private static final String URL_DEFAULT = "/controller?command=empty";
    private static final String COMMAND = "showquestionform";
    private static final Logger LOG = LogManager.getLogger(ShowQuestionFormCommand.class);

    public Command getCommand(HttpServletRequest request) throws CommandException {
        if(request.getParameter("command").equals(COMMAND)) {
            return new ShowQuestionFormCommand();
        } else {
            return new LogInCommand().getCommand(request);
        }
    }

    /**
     * Return URL for question form's page.
     * @param request request from client
     * @param response HttpServletResponse
     * @return new page's URL.
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        try {
            List<Theme> list = new LinkedList<>();
            list = ThemeService.getAll();
            LOG.debug(list);
            request.setAttribute(REQUEST_ATTR_THEMES, list);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return Optional.of("/jsp/ask_question.jsp");
    }
}
