package edu.epam.likeit.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *  Data Access Object for wring/reading answer marks to/from database.
 */
public class MarksDAO extends AbstractDAO {
    private static final String INSERT_MARK
            = "INSERT INTO MARKS (ANSWER_ID, USER_ID, VALUE) " +
            "VALUES (?, ?, ?)";
    private static final String SELECT_BY_USER_AND_ANSWER = "SELECT * FROM MARKS WHERE USER_ID=? AND ANSWER_ID=?";
    private static final String DELETE_BY_USER_AND_ANSWER = "DELETE FROM MARKS WHERE USER_ID=? AND ANSWER_ID=?";
    private static final Logger LOG = LogManager.getLogger(MarksDAO.class);


    public MarksDAO() throws DAOException {

    }

    /**
     * Check whether the assessment is already set and determines further actions.
     * @param userId user that set assessment.
     * @param answerId {@link edu.epam.likeit.entity.Answer} that received an mark.
     * @param value is the mark. Can be true (+1) or false (-1).
     * @throws DAOException
     */
    public void checkMark(long userId, long answerId, boolean value) throws DAOException{
        AnswerDAO dao = new AnswerDAO();
        int recentMark = takeUserMark(userId, answerId);
        if(recentMark == 0) {
            putMark(userId, answerId, value);
            dao.changeRating(value, answerId);
        } else {
            if((recentMark == 1)^value) {
                deleteMark(userId, answerId);
                dao.changeRating(value, answerId);
            } else {
                return;
            }
        }
    }

    /**
     * Puts mark into database.
     * @param userId user that set assessment.
     * @param answerId {@link edu.epam.likeit.entity.Answer} that received an mark.
     * @param value is the mark. Can be true (+1) or false (-1).
     * @throws DAOException
     */
    public void putMark(long userId, long answerId, boolean value) throws DAOException{
        String generated[] = {"ID"};
        try (PreparedStatement ps = connection.prepareStatement(INSERT_MARK, generated)) {
            ps.setLong(1, answerId);
            ps.setLong(2, userId);
            ps.setBoolean(3, value);
            ps.executeUpdate();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (!generatedKeys.next()) {
                throw new DAOException("No ID obtained");
            }
        } catch (SQLException e) {
            throw new DAOException("Error in putMark()", e);
        }
    }

    /**
     * Deletes mark from database.
     * @param userId user that set assessment.
     * @param answerId {@link edu.epam.likeit.entity.Answer} that received an mark.
     * @throws DAOException
     */
    public void deleteMark(long userId, long answerId) throws DAOException{
        try (PreparedStatement ps = connection.prepareStatement(DELETE_BY_USER_AND_ANSWER)) {
            ps.setLong(1, userId);
            ps.setLong(2, answerId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Error in deleteMark()", e);
        }

    }

    /**
     * Check is the user assessed this answer.
     * @param userId
     * @param answerId
     * @return 0 when there is no mark, 1 if user mark is +1, -1 if user mark is -1.
     * @throws DAOException
     */
    public int takeUserMark(long userId, long answerId) throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_BY_USER_AND_ANSWER)) {
                ps.setLong(1, userId);
            ps.setLong(2, answerId);
            ResultSet rs = ps.executeQuery();
            LOG.debug("DAOMarks: "  + userId + " " + answerId);
            if(rs.next()) {
                if(rs.getBoolean("VALUE")) {
                    return 1;
                } else {
                    return -1;
                }
            } else {
                return 0;
            }
        } catch (SQLException e) {
            throw new DAOException("Error in isUserAssessed()", e);
        }
    }


}
