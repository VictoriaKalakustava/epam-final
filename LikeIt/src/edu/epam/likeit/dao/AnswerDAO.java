package edu.epam.likeit.dao;

import edu.epam.likeit.entity.Answer;
import edu.epam.likeit.entity.User;
import edu.epam.likeit.service.ServiceException;
import edu.epam.likeit.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 * Data Access Object for wring/reading {@link Answer} to/from database.
 */
public class AnswerDAO extends AbstractDAO {
    private static final String INSERT_ANSWER
            = "INSERT INTO ANSWER (QUESTION_ID, TEXT, USER_ID, CREATING_TIME, RATING) " +
            "VALUES (?, ?, ?, ?, ?)";
    private static final String SELECT_BY_QUESTION_ID = "SELECT * FROM ANSWER WHERE QUESTION_ID=?";
    private static final String CHANGE_RATING = "UPDATE ANSWER SET RATING = RATING + ? WHERE ID=?";
    private static final String DELETE_ANSWER_BY_ID = "DELETE FROM ANSWER WHERE ID=?";
    private static final String UPDATE_ANSWER ="UPDATE ANSWER SET TEXT=?" +
                                            "WHERE ID=?";
    private static final Logger LOG = LogManager.getLogger();

    public AnswerDAO() throws DAOException {}

    /**
     * Updates answer's text into database.
     * @param answer answer with new text
     * @return updated answer
     * @throws DAOException
     */
    public Answer update(Answer answer) throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(UPDATE_ANSWER)) {
            ps.setString(1, answer.getAnswer());
            ps.setLong(2, answer.getId());
            ps.executeUpdate();
            return answer;
        } catch (SQLException e) {
            throw new DAOException("Error in update()", e);
        }
    }

    /**
     * Deletes answer from database.
     * @param id user id to delete it from database.
     * @throws DAOException
     */
    public void deleteAnswer(long id) throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_ANSWER_BY_ID)) {
            ps.setLong(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Error in delete()", e);
        }
    }

    /**
     * Finds all answers for question.
     * @param id question id
     * @return list with all answers for the question
     * @throws DAOException
     * @throws ServiceException
     */
    public LinkedList<Answer> findByQuestionId(long id) throws DAOException, ServiceException {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_BY_QUESTION_ID)) {
            ps.setLong(1, id);
            ResultSet resultSet = ps.executeQuery();
            LinkedList<Answer> answers = new LinkedList<>();
            while(resultSet.next()) {
                User user = UserService.getUserById(resultSet.getLong("USER_ID"));
                answers.add(new Answer(resultSet.getLong("ID"), resultSet.getLong("QUESTION_ID"),
                        resultSet.getString("TEXT"), user, resultSet.getDate("CREATING_TIME"),
                        resultSet.getLong("RATING")));
            }
            if(answers.isEmpty()) {
                return null;
            } else {
                return answers;
            }
        } catch (SQLException e) {
            throw new DAOException("Error in findByQuestionId()", e);
        }
    }

    /**
     * Writes new answer into database.
     * @param answer
     * @return created answer with new id.
     * @throws DAOException
     */
    public Answer createAnswer(Answer answer) throws DAOException {
        String generated[] = {"ID"};
        try (PreparedStatement ps = connection.prepareStatement(INSERT_ANSWER, generated)) {
            ps.setLong(1, answer.getQuestionId());
            ps.setString(2, answer.getAnswer());
            ps.setLong(3, answer.getUser().getId());
            ps.setDate(4, new java.sql.Date(System.currentTimeMillis()));
            ps.setLong(5, 0);
            ps.executeUpdate();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                answer.setId(generatedKeys.getLong(1));
            } else {
                throw new DAOException("No ID obtained");
            }
            return answer;
        } catch (SQLException e) {
            throw new DAOException("Error in createNewAnswer()", e);
        }
    }

    /**
     * Changes anser rating for 1 or -1.
     * @param mark true when increment, false when decrement
     * @param answerId id answer which estimated
     * @throws DAOException
     */
    public void changeRating(boolean mark, long answerId) throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(CHANGE_RATING)) {
            if(mark) {
                ps.setInt(1, 1);
            } else {
                ps.setInt(1, -1);
            }
            ps.setLong(2, answerId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Error in changeRating()", e);
        }
    }
}
