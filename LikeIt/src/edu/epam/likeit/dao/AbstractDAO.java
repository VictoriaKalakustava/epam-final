package edu.epam.likeit.dao;


import edu.epam.likeit.connectionpool.ConnectionPool;
import edu.epam.likeit.connectionpool.ConnectionPoolException;
import edu.epam.likeit.connectionpool.ProxyConnection;

/**
 * Abstract class for all DAOs, implementing AutoCloseable.
 */
abstract class AbstractDAO implements AutoCloseable {
    ProxyConnection connection;

    /**
     * Gets new connection from Connection Pool.
     * @throws DAOException
     */
    AbstractDAO() throws DAOException {
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (ConnectionPoolException e) {
            throw new DAOException(e);
        }
    }

    /**
     * Returns DAO's connection to pool.
     */
    @Override
    public void close() {
        ConnectionPool.getInstance().releaseConnection(connection);
    }
}
