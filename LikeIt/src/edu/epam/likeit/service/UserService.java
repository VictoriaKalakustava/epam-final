package edu.epam.likeit.service;

import edu.epam.likeit.dao.DAOException;
import edu.epam.likeit.dao.UserDAO;
import edu.epam.likeit.entity.User;
import edu.epam.likeit.util.MD5Digest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Service for working with {@link User}s.
 */
public class UserService {

    public static final Logger LOG = LogManager.getLogger(UserService.class);

    /**
     * Writes new {@link User} into database.
     * @param user
     * @return {@link User} with correct id.
     * @throws ServiceException
     */
    public static User registerNewUser(User user) throws ServiceException {
        try(UserDAO dao = new UserDAO()) {
            boolean isLoginFree = dao.isLoginFree(user.getLogin());
            if(isLoginFree) {
                user.setPassword(MD5Digest.encrypt(user.getPassword()));
                LOG.debug("return true");
                return dao.registerNewUser(user);
            } else {
                LOG.debug("return false");
                return null;
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds {@link User} by id.
     * @param id
     * @return user
     * @throws ServiceException
     */
    public static User getUserById(long id) throws ServiceException {
        try(UserDAO dao = new UserDAO()) {
            return dao.findById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds user by login and password.
     * @param login
     * @param password
     * @return {@link User} with full information or null if there is
     * no such user
     * @throws ServiceException
     */
    public static User getByData(String login, String password) throws ServiceException {
        try(UserDAO dao = new UserDAO()) {
            return dao.findByData(login, password);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Changes rating.
     * @param userId
     * @param mark
     * @throws ServiceException
     */
    public static void changeRating(long userId, boolean mark) throws ServiceException {
        try(UserDAO dao = new UserDAO()) {
            if(mark) {
                dao.changeRating(userId, 1);
            } else {
                dao.changeRating(userId, -1);

            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Updates user information
     * @param user
     * @throws ServiceException
     */
    public static void updateUser(User user) throws ServiceException {
        try(UserDAO dao = new UserDAO()) {
            dao.update(user);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Checks is this login free.
     * @param login
     * @return true if it's free
     * @throws ServiceException
     */
    public static boolean isLoginFree(String login) throws ServiceException {
        try(UserDAO dao = new UserDAO()) {
            return dao.isLoginFree(login);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Deletes user from database.
     * @param user
     * @throws ServiceException
     */
    public static void deleteUser(User user) throws ServiceException {
        try(UserDAO dao = new UserDAO()) {
            dao.delete(user.getId());
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
