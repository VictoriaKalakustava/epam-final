package edu.epam.likeit.service;


import edu.epam.likeit.dao.DAOException;
import edu.epam.likeit.dao.ThemeDAO;
import edu.epam.likeit.entity.Theme;

import java.util.List;

/**
 * Service for working with {@link Theme}s.
 */
public class ThemeService {
    /**
     * Writes new theme into database.
     * @param theme
     * @return {@link Theme} with correct id.
     * @throws ServiceException
     */
    public static Theme create(Theme theme) throws ServiceException {
        try(ThemeDAO dao = new ThemeDAO()) {
            return dao.create(theme);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds theme by id.
     * @param id
     * @return theme
     * @throws ServiceException
     */
    public static Theme findById(int id) throws ServiceException {
        try(ThemeDAO dao = new ThemeDAO()) {
            return dao.findById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @return all themes from database
     * @throws ServiceException
     */
    public static List<Theme> getAll() throws ServiceException {
        try(ThemeDAO dao = new ThemeDAO()) {
            return dao.getAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
