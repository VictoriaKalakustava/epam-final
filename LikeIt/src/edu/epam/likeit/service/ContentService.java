package edu.epam.likeit.service;

import edu.epam.likeit.dao.ContentDAO;
import edu.epam.likeit.dao.DAOException;
import edu.epam.likeit.entity.Content;

/**
 * Service for working with {@link Content}
 */
public class ContentService {
    /**
     * Deletes question's content from database.
     * @param questionId
     * @throws ServiceException
     */
    public void delete(long questionId) throws ServiceException {
        try(ContentDAO contentDAO = new ContentDAO()) {
            contentDAO.delete(questionId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Writes question content into database.
     * @param content
     * @throws ServiceException
     */
    public void insert(Content content) throws ServiceException {
        try(ContentDAO contentDAO = new ContentDAO()) {
            contentDAO.insertContent(content);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds content for the question
     * @param questionId
     * @return content
     * @throws ServiceException
     */
    public static Content findByQuestionId(long questionId) throws ServiceException {
        try(ContentDAO contentDAO = new ContentDAO()) {
            return contentDAO.findByQuestionId(questionId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Update question content.
     * @param content
     * @return updated content
     * @throws ServiceException
     */
    public Content update(Content content) throws ServiceException {
        try(ContentDAO contentDAO = new ContentDAO()) {
            return contentDAO.update(content);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
