package edu.epam.likeit.service;

import edu.epam.likeit.dao.ContentDAO;
import edu.epam.likeit.dao.DAOException;
import edu.epam.likeit.dao.QuestionDAO;
import edu.epam.likeit.entity.Content;
import edu.epam.likeit.entity.Question;

import java.util.LinkedList;

/**
 * Service for working with {@link Question}s.
 */
public class QuestionService {
    /**
     * Deletes question from database.
     * @param question
     * @throws ServiceException
     */
    public static void delete(Question question) throws ServiceException {
        try (QuestionDAO questionDAO = new QuestionDAO()){
            ContentDAO contentDAO = new ContentDAO();
            contentDAO.delete(question.getId());
            questionDAO.delete(question.getId());
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Writes question into database.
     * @param content
     * @param question
     * @return question with correct id.
     * @throws ServiceException
     */
    public static Question insert(Content content, Question question) throws ServiceException {
        try(QuestionDAO questionDAO = new QuestionDAO()) {
            ContentService contentService = new ContentService();
            question = questionDAO.createNewQuestion(question);
            content.setQuestionId(question.getId());
            contentService.insert(content);
            return question;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds all questions.
     * @return list of questions
     * @throws ServiceException
     */
    public static LinkedList<Question> findAll() throws ServiceException {
        try(QuestionDAO questionDAO = new QuestionDAO()) {
            return questionDAO.findAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds question by its id.
     * @param questionId
     * @return question
     * @throws ServiceException
     */
    public static Question findByQuestionId(long questionId) throws ServiceException {
        try(QuestionDAO questionDAO = new QuestionDAO()) {
            return questionDAO.findById(questionId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Updates question.
     * @param question
     * @param content
     * @return updated question.
     * @throws ServiceException
     */
    public Question update(Question question, Content content) throws ServiceException {
        try(QuestionDAO questionDAO = new QuestionDAO()) {
            ContentService contentService = new ContentService();
            contentService.update(content);
            return questionDAO.update(question);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
