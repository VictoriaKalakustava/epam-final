package edu.epam.likeit.service;

import edu.epam.likeit.dao.DAOException;
import edu.epam.likeit.dao.MarksDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Service for associate users and its marks.
 */
public class MarkService {
    public static final Logger LOG = LogManager.getLogger(MarkService.class);
    public static void putMark(long userId, long authorId, long answerId, boolean mark) throws ServiceException {
        try {
            MarksDAO marksDAO = new MarksDAO();
            AnswerService answerService = new AnswerService();
            UserService userService = new UserService();
            int recentMark = marksDAO.takeUserMark(userId, answerId);
            if(recentMark == 0) {
                marksDAO.putMark(userId, answerId, mark);
            } else {
                if((recentMark == 1)^mark) {
                    marksDAO.deleteMark(userId, answerId);

                } else {
                    return;
                }
            }
            answerService.changeAnswerRating(mark, answerId);
            userService.changeRating(authorId, mark);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
