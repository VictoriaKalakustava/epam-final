package edu.epam.likeit.service;

import edu.epam.likeit.dao.AnswerDAO;
import edu.epam.likeit.dao.DAOException;
import edu.epam.likeit.entity.Answer;

import java.util.List;

/**
 * Service for working with {@link Answer}s.
 */
public class AnswerService {
    /**
     * Writes new answer into database
     * @param answer
     * @return answer with correct id
     * @throws ServiceException
     */
    public static Answer createAnswer(Answer answer) throws ServiceException {
        try(AnswerDAO dao = new AnswerDAO()) {
            return dao.createAnswer(answer);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Remove answer from database by id.
     * @param id
     * @throws ServiceException
     */
    public static void deleteAnswer(long id) throws ServiceException {
        try(AnswerDAO dao = new AnswerDAO()) {
            dao.deleteAnswer(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public static void deleteQuestionsAnswers(long questionId) throws ServiceException {
        try(AnswerDAO dao = new AnswerDAO()) {
            for (Answer answer : dao.findByQuestionId(questionId)) {
              dao.deleteAnswer(answer.getId());
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds all answer for this question.
     * @param questionId
     * @return {@code List<Answer>} is all answers for the question.
     * @throws ServiceException
     */
    public static List<Answer> findAllAnswers(long questionId) throws ServiceException {
        try(AnswerDAO dao = new AnswerDAO()) {
            return dao.findByQuestionId(questionId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Changes answer rating for +1 or -1.
     * @param mark when its true - rating decrements, when its false - decrement.
     * @param answerId
     * @throws ServiceException
     */
    public void changeAnswerRating(boolean mark, long answerId) throws ServiceException {
        try(AnswerDAO dao = new AnswerDAO()) {
            dao.changeRating(mark, answerId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Updates answer information.
     * @param answer
     * @throws ServiceException
     */
    public void changeAnswer(Answer answer) throws ServiceException {
        try(AnswerDAO dao = new AnswerDAO()) {
            dao.update(answer);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
