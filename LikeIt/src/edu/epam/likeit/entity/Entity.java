package edu.epam.likeit.entity;

/**
 * Abstract class for all system's entities
 */
abstract class Entity {
    protected long id;

    public Entity(long id) {
        this.id = id;
    }

    public Entity() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
