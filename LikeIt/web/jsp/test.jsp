<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title</title>
    <!-- Bootstrap Core CSS -->
    <link href="resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="resources/bootstrap/css/blog-home.css" rel="stylesheet">

    <link href="resources/bootstrap/css/test.css" rel="stylesheet">
</head>
<body>
<div class="container">

    <div style="width: 40px; text-align: center;">
        <svg width="20px" height="10px"><polygon style="cursor: pointer" id="up" points="10,0 20,10 0,10" fill="#a5a5a5"/></svg>
        <div class="story__rating-count">1250</div>
        <svg width="20px" height="10px"><polygon style="cursor: pointer" id="down" points="0,0 20,0 10,10" fill="#a5a5a5"/></svg>
    </div>
</div>
<!-- /.container -->

<!-- jQuery -->
<script src="resources/bootstrap/js/jquery.js"></script>
<script src="resources/bootstrap/js/mark.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="resources/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
